﻿using System;
using System.Collections.Generic;
using AEX.VegetationSpawner.CompProperties;
using RimWorld;
using Verse;

namespace AEX.VegetationSpawner.Comp {
    public class RadialSpawner : ThingComp {
        public CompProperties.RadialSpawner Properties => (CompProperties.RadialSpawner)this.props;
        protected int ticksUntilNextSpawn = 0;
        private List<IntVec3> cachedCellList;
        private IEnumerator<IntVec3> cellIterator;

        public override void CompTick() {
            handleTicks(1);
        }

        public override void CompTickRare() {
            handleTicks(GenTicks.TickRareInterval);
        }

        private void handleTicks(int ticks) {
            var powerComp = parent.TryGetComp<CompPowerTrader>();
            if (powerComp != null && !powerComp.PowerOn) { return; }

            ticksUntilNextSpawn -= ticks;

            if (ticksUntilNextSpawn <= 0) {
                spawnItem();
            }
        }

        public override void PostSpawnSetup(bool respawningAfterLoad) {
            if (!respawningAfterLoad) {
                cacheCellList();
                resetCountdown();
            }
        }

        protected void cacheCellList() {
            cachedCellList = new List<IntVec3>();

            int x = 0;
            int z = 0;
            var d = Properties.radius * 2;
            var dSquared = d * d;

            for (int i = 0; i < dSquared; i++) {
                int xp = x + d / 2;
                int yp = z + d / 2;
                if (xp >= 0 && xp < d && yp >= 0 && yp < d) {
                    var cell = new IntVec3(x, 0, z);
                    if (cell.DistanceTo(IntVec3.Zero) < Properties.radius) {
                        cachedCellList.Add(cell);
                    }
                }

                if (Math.Abs(x) <= Math.Abs(z) && (x != z || x >= 0)) {
                    x += ((z >= 0) ? 1 : -1);
                } else {
                    z += ((x >= 0) ? -1 : 1);
                }
            }
        }

        private void spawnItem() {
            var newItem = ThingMaker.MakeThing(Properties.thingToSpawn);
            newItem.stackCount = Properties.spawnCount;
            var placed = spawnItem(parent.Map, parent.Position, newItem);
            if (placed != null) {
                PostSpawn(placed);
            }
                
            resetCountdown();
        }

        protected virtual void PostSpawn(Thing t) {
            
        }

        protected virtual bool CheckTile(Map map, IntVec3 cell, Thing newItem) {
            if (!cell.Walkable(map)) { return false; }
            var edifice = cell.GetEdifice(parent.Map);
            if (edifice != null && !Properties.thingToSpawn.IsEdifice()) { return false; }
            var door = edifice as Building_Door;
            if (door != null && !door.FreePassage) { return false; }

            var things = cell.GetThingList(map);
            foreach (var thing in things) {
                if (thing.def == Properties.thingToSpawn) {
                    var count = thing.stackCount;
                    var max = newItem.def.stackLimit;
                    if (count >= max) { return false; }
                }
            }

            return true;
        }

        private Thing spawnItem(Map map, IntVec3 center, Thing newItem) {
            int count = cachedCellList.Count;
            while (count >= 0) {
                if (cellIterator == null || !cellIterator.MoveNext()) {
                    switch (Properties.nextCellChoice) {
                        case NextCellChoice.Random:
                            cellIterator = cachedCellList.InRandomOrder().GetEnumerator();
                            break;
                        case NextCellChoice.Spiral:
                            cellIterator = cachedCellList.GetEnumerator();
                            break;
                    }
                    cellIterator.MoveNext();
                }
                var cell = center + cellIterator.Current;

                Thing outThing = null;
                if (CheckTile(map, cell, newItem)) {
                    if (GenPlace.TryPlaceThing(newItem, cell, map, ThingPlaceMode.Direct, out outThing)) {
                        return outThing;
                    }
                } else if (Properties.occupiedCellBehavior == OccupiedCellBehavior.Skip) {
                    return outThing;
                }
                count--;
            }

            return null;
        }


        private void resetCountdown() {
            ticksUntilNextSpawn = Properties.spawnIntervalRange.RandomInRange;
        }

        public override string CompInspectStringExtra() {
            string ret = "";

            if (Properties.writeTimeLeftToSpawn) {
                ret = $"{ticksUntilNextSpawn.ToStringTicksToPeriod(true, true)} until next spawn.";
            }

            return ret;
        }
    }
}
