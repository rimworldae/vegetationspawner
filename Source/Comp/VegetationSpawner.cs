﻿using System;
using RimWorld;
using Verse;

namespace AEX.VegetationSpawner.Comp {
    public class VegetationSpawner : RadialSpawner {
        protected override void PostSpawn(Thing t) =>
            (t as Plant).Growth = (Properties as CompProperties.VegetationSpawner).percentGrown;

        protected override bool CheckTile(Map map, IntVec3 cell, Thing newItem) {
            var isFertile = map.fertilityGrid.FertilityAt(cell) > 0;
            return isFertile && base.CheckTile(map, cell, newItem);
        }

        public override string CompInspectStringExtra() {
            string ret = "";

            if (Properties.writeTimeLeftToSpawn) {
                ret = $"{ticksUntilNextSpawn.ToStringTicksToPeriod(true, true)} until next plant.";
            }

            return ret;
        }
    }
}
