﻿using System;
using System.Collections.Generic;
using Verse;

namespace AEX.VegetationSpawner.CompProperties {
    public enum OccupiedCellBehavior {
        Skip,
        Next, 
    }

    public enum NextCellChoice {
        Random,
        Spiral
    }

    public class RadialSpawner : Verse.CompProperties {
        public RadialSpawner() {
            this.compClass = typeof(Comp.RadialSpawner);
            occupiedCellBehavior = OccupiedCellBehavior.Next;
            nextCellChoice = NextCellChoice.Random;
        }

        public ThingDef thingToSpawn;
        public int radius;
        public int spawnCount;
        public IntRange spawnIntervalRange = new IntRange(100, 100);
        public NextCellChoice nextCellChoice;
        public OccupiedCellBehavior occupiedCellBehavior;
        public bool writeTimeLeftToSpawn;
        public bool showMessageIfOwned;
    }
}
