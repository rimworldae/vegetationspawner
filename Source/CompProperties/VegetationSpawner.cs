﻿using System;
namespace AEX.VegetationSpawner.CompProperties {
    public class VegetationSpawner : RadialSpawner {
        public VegetationSpawner() {
            compClass = typeof(Comp.VegetationSpawner);
        }

        public float percentGrown;
    }
}
